export const indexToWeekDay = (index: number): string => {
  const weekdays = [
    'Mandag',
    'Tirsdag',
    'Onsdag',
    'Torsdag',
    'Fredag',
    'Lørdag',
    'Søndag'
  ];
  return weekdays[index];
};

export const formatStartEndTime = (
  startTime: number,
  endTime?: number
): string => {
  return `${decimalToTime(startTime)} ${
    endTime ? `- ${decimalToTime(endTime)}` : ''
  }`;
};

const decimalToTime = (decimal: number): string => {
  const time = decimal.toString();

  // format time with colon delimiter
  if (time.indexOf(':') === -1) {
    if (time.length === 1 || time.length === 2) {
      return `${time}:00`;
    } else if (time.length === 3) {
      return `${time.slice(0, 1)}:${time.slice(1)}`;
    } else if (time.length === 4) {
      return `${time.slice(0, 2)}:${time.slice(2)}`;
    }
  }

  return time;
};
