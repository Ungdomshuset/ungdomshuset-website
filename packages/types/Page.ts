export type Page = {
  _id: string;
  title: string;
  slug: {
    current: string;
  };
  image: any;
  content: any;
  seo: {
    metaTitle: string;
    metaDescription: string;
    ogTitle: string;
    ogDescription: string;
    ogImage: any;
  };
};
