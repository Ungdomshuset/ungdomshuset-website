export type NewsItem = {
  _id: string;
  title: string;
  date: string;
  image: any;
  slug: {
    _type: string;
    current: string;
  };
  content: any;
};
