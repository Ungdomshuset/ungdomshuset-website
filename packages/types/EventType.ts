export type EventType = {
  _id: string;
  name: string;
};
