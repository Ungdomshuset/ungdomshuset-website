# The Ungeren Website

This is the documentation for the Ungeren website. It provides an overview of how the website is developed, build and deployed, without going into details about the individual frameworks used.

## Setup

`Sanity`: Is a headless CMS hosted in the cloud. It is used for adding and updating the website content. Eg. adding a new show to the calendar, changing the weekly activities etc.

`Next.js`: Is a "meta framework" built on top of the React framework. It comes with routing and it able to generate static pages at build time.

`Turborepo`: Turborepo is a build and bundler system. It is used during local development and when building out the website for production. See the `tubrorepo.md` file for details.

`TailwindCSS`: Tailwind is a utility-first CSS framework. Contrary to other CSS frameworks, like Bootstrap, it doesn't come with predefined components.

`Framer Motion`: Framer Motion is an animatiopn library. It is primarily used for page transitions on the Ungeren website.

`GitHub`: The website source code is hosted with GitHub.

`Vercel`: The website is hosted with Vercel. Do note that the Sanity Studio is hosted separately.
