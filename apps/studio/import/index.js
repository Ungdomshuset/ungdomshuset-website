const jsdom = require('jsdom')
const ndjson = require('ndjson')
const fs = require('fs')
const shows = require('./raw.js')
const {Schema} = require('@sanity/schema')
const blockTools = require('@sanity/block-tools')

const defaultSchema = Schema.compile({
  name: 'withDefaultBlockType',
  types: [
    {
      type: 'object',
      name: 'event',
      fields: [
        {
          title: 'Title',
          type: 'string',
          name: 'title',
        },
        {
          title: 'Body',
          name: 'pageContent',
          type: 'array',
          of: [
            {
              type: 'block',
              styles: [
                {title: 'Normal', value: 'normal'},
                {title: '2. header (xxlarge, orange)', value: 'h2'},
                {title: '3. header (xlarge, orange)', value: 'h3'},
                {title: '4. header (large, black)', value: 'h4'},
              ],
              marks: {
                decorators: [
                  {title: 'Strong', value: 'strong'},
                  {title: 'Emphasis', value: 'em'},
                ],
                annotations: [
                  {
                    title: 'URL',
                    name: 'link',
                    type: 'object',
                    fields: [
                      {
                        title: 'URL',
                        name: 'href',
                        type: 'url',
                      },
                    ],
                  },
                ],
              },
              lists: [
                {title: 'Bullet', value: 'bullet'},
                {title: 'Number', value: 'number'},
              ],
            },
          ],
        },
      ],
    },
  ],
})

const blockContentType = defaultSchema
  .get('event')
  .fields.find((field) => field.name === 'pageContent').type

const {JSDOM} = jsdom
let ndJsonLines = ''
const ndSerialize = ndjson.stringify()
ndSerialize.on('data', (line) => (ndJsonLines += `${line}`))
const writeJson = (obj) => ndSerialize.write(obj)

async function main() {
  const posts = shows;

  for (const post of posts) {
    await processPost(post)
    console.log('Saved', post.id)
  }

  fs.writeFileSync('import/output.ndjson', ndJsonLines, (err) => {
    if (err) throw err
    console.log('The file has been saved!')
  })
}

main()

async function processPost(post) {
  let { id, title, date, price, url, text } = post
  
  // Remove whitespace around linebreaks in the title
  title = title.replace(/\s*\n\s*/g, '\n')

  let html = text
  html = html.replace(/&amp;/g, '&')
  html = html.replace(/&lt;/g, '<')
  html = html.replace(/&gt;/g, '>')
  html = html.replace(/&quot;/g, '"')
  html = html.replace(/&#039;/g, "'")
  html = html.replace(/(\^\^\^)/g, '"')

  // find first image in html and use that as main image
  let image = undefined
  const firstImage = html.match(/<img[^>]+>/)
  if (firstImage && firstImage.length > 0 && firstImage[0].includes('src')) {
    const imageValue = firstImage[0].match(/src="([^"]+)"/)
    if (imageValue && imageValue.length > 1) {
      let src = imageValue[1]

      // Skip images that contains "fbcdn"
      const ignore = [
        'fbcdn',
        'akpress',
        'pinnerup.anarcho',
        'gif_balkan_boogie_circusnight',
        'metal-for-asyl',
        'img375.imageshack.us',
      ]
      if (!ignore.some((ignore) => src.includes(ignore))) {
        // if source is relative, add domain
        if (!src.startsWith('http')) {
          src = `http://ungeren.dk${src}`
        }

        src = src.replace(/&oslash;/g, 'ø')
        src = src.replace(/&aring;/g, 'å')
        src = src.replace(/&Aring;/g, 'Å')
        src = src.replace(/&aelig;/g, 'æ')
        src = src.replace(/&uuml;/g, 'ü')

        image = {_type: 'image', _sanityAsset: `image@${src}`}
      }
    }
  }

  // Remove all images from html
  html = html.replace(/<img[^>]+>/g, '')
  html = html.replace(/<p><\/p>/g, '')
  html = html.replace(/<p>\s*<\/p>/g, '')
  html = html.replace(/<p>\s*<br>\s*<\/p>/g, '')

  // Trim of whitespace and newlines from the start and end of the html
  html = html.replace(/^\s+|\s+$/g, '')


  let content = blockTools.htmlToBlocks(html, blockContentType, {
    parseHtml: (html) => {
      // Remove the first tag if it is empty
      var doc = new JSDOM(html);
      var firstTag = doc.window.document.body.firstChild;
      if (firstTag && firstTag.tagName == "P" &&
        (firstTag.innerHTML == "" || firstTag.innerHTML == "&nbsp;")
      ) {
        firstTag.remove();
      }
      return doc.window.document;
    }
  })

  // Remove empty first and last block
  content = content.filter((block, index) => {
    if (
      (index === 0 || index === content.length - 1) &&
      block.children.length === 1 &&
      block.children[0]._type === 'span' &&
      block.children[0].text === ''
    ) {
      return false
    }

    if (
      block.children.length === 1 &&
      block.children[0]._type === 'span' &&
      block.children[0].text === '\n'
    ) {
      return false
    }

    return true
  })

  writeJson({
    _id: `show-${id}`,
    _type: 'event',
    title,
    date,
    image,
    slug: {_type: 'slug', current: url},
    price,
    content,
  })
}

// async function convertHtmlToBlock(html) {
//   return blockTools.htmlToBlocks(html, blockContentType, {
//     parseHtml: (html) => {
//       return new JSDOM(`<!DOCTYPE html><html><body>${html}</body></html>`).window.document
//     },
//     // rules: [
//     //   {
//     //     deserialize(el, next, block) {
//     //       if (el?.tagName?.toLowerCase() === 'img') {
//     //         return block({
//     //           _type: 'image',
//     //           _sanityAsset: `image@http://ungeren.dk${el.getAttribute('src')}`,
//     //         })
//     //       }
//     //       return undefined
//     //     },
//     //   },
//     // ],
//   })
// }
