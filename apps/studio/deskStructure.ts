import {BsMegaphone} from 'react-icons/bs'
import {CiCalendar, CiRepeat, CiSettings, CiStickyNote} from 'react-icons/ci'
import {StructureBuilder} from 'sanity/desk'

export const deskStructure = (S: StructureBuilder) =>
  S.list()
    .title('Content')
    .items([
      S.listItem()
        .title('Upcoming events')
        .icon(CiCalendar)
        .schemaType('event')
        .child(
          S.documentList()
            .title('Events')
            .filter('_type == "event" && date >= now()')
            .defaultOrdering([{field: 'date', direction: 'asc'}])
        ),

      S.listItem()
        .title('Full calendar')
        .icon(CiCalendar)
        .child(
          S.list()
            .title('Full calendar')
            .items([
              S.listItem()
                .title('Archived events')
                .icon(CiCalendar)
                .schemaType('event')
                .child(
                  S.documentList()
                    .title('Events')
                    .filter('_type == "event" && date < now()')
                    .defaultOrdering([{field: 'date', direction: 'desc'}])
                ),

              S.listItem()
                .title('Recurring events')
                .icon(CiRepeat)
                .schemaType('recurring_event')
                .child(
                  S.documentList()
                    .title('Recurring events')
                    .filter('_type == "recurring_event"')
                    .defaultOrdering([
                      {field: 'weekday', direction: 'asc'},
                      {field: 'startTime', direction: 'asc'},
                    ])
                ),

              S.documentTypeListItem('event_type').title('Event types'),
            ])
        ),

      S.listItem()
        .title('News')
        .icon(BsMegaphone)
        .schemaType('event')
        .child(
          S.documentList()
            .title('News')
            .filter('_type == "news"')
            .defaultOrdering([{field: 'date', direction: 'desc'}])
        ),

      S.documentTypeListItem('page').title('Pages'),

      S.documentTypeListItem('banner').title('Banners'),

      S.listItem()
        .title('Settings')
        .icon(CiSettings)
        .child(S.document().schemaType('siteSettings').documentId('siteSettings')),
    ])
