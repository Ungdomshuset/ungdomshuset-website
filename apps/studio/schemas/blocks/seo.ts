import {defineField, defineType} from 'sanity'

export default defineType({
  title: 'SEO',
  name: 'seo',
  type: 'object',
  fields: [
    defineField({
      title: 'Meta title',
      name: 'metaTitle',
      description: "Shown in the browser's tab",
      type: 'string',
    }),
    defineField({
      title: 'Meta description',
      name: 'metaDescription',
      description: 'Shown in search engines',
      type: 'text',
    }),
    defineField({
      title: 'Open Graph Title',
      name: 'ogTitle',
      description: 'Shown in social media',
      type: 'string',
    }),
    defineField({
      title: 'Open Graph Description',
      name: 'ogDescription',
      description: 'Shown in social media',
      type: 'text',
    }),
    defineField({
      title: 'Open Graph Image',
      name: 'ogImage',
      description: 'Shown in social media',
      type: 'image',
    }),
  ],
})
