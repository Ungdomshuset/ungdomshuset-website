import format from 'date-fns/format'
import parseISO from 'date-fns/parseISO'
import {CiCalendar} from 'react-icons/ci'
import {defineField, defineType} from 'sanity'
import {stringToSlug} from '../helpers/slug-helper'

export default defineType({
  name: 'event',
  type: 'document',
  icon: CiCalendar,
  title: 'Event',
  fields: [
    defineField({
      title: 'Title',
      name: 'title',
      type: 'text',
      rows: 3,
    }),
    defineField({
      title: 'Date',
      name: 'date',
      type: 'datetime',
    }),
    defineField({
      title: 'Price',
      name: 'price',
      type: 'string',
      validation: (Rule) => Rule.max(25),
    }),
    defineField({
      title: 'Type',
      name: 'event_type',
      type: 'reference',
      to: [{type: 'event_type'}],
    }),
    defineField({
      title: 'Place',
      name: 'place',
      type: 'string',
      description: 'Dødsmaskinen, Svederen etc.',
      validation: (Rule) => Rule.max(25),
    }),
    defineField({
      title: 'Image',
      name: 'image',
      type: 'image',
    }),
    defineField({
      title: 'Slug',
      name: 'slug',
      type: 'slug',
      options: {
        source: (doc: any) => {
          const date = format(parseISO(doc.date), 'dd-MM-yyyy')
          const title = stringToSlug(doc.title).slice(0, 200)
          return date + '-' + title
        },
        slugify: (input) => 'kalender/' + input,
      },
      validation: (Rule: any) => Rule.required(),
    }),
    defineField({
      title: 'Legacy Slug',
      name: 'legacy_slug',
      type: 'string',
    }),
    defineField({
      title: 'Artists',
      name: 'artists',
      type: 'array',
      of: [{type: 'artist'}],
    }),
    defineField({
      title: 'Content',
      name: 'content',
      type: 'pageContent',
    }),
  ],
  preview: {
    select: {
      title: 'title',
      date: 'date',
    },
    prepare(selection: any) {
      const {title, date} = selection
      return {
        title: title,
        subtitle: format(parseISO(date), 'EEEE d MMM. yyyy'),
      }
    },
  },
  // orderings: [
  //   {
  //     title: 'Show date, New',
  //     name: 'dateDesc',
  //     by: [{field: 'date', direction: 'desc'}],
  //   },
  //   {
  //     title: 'Show, Old',
  //     name: 'dateAsc',
  //     by: [{field: 'date', direction: 'asc'}],
  //   },
  // ],
})
