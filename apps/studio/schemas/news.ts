import format from 'date-fns/format'
import parseISO from 'date-fns/parseISO'
import {BsMegaphone} from 'react-icons/bs'
import {defineField, defineType} from 'sanity'
import {stringToSlug} from '../helpers/slug-helper'

export default defineType({
  name: 'news',
  type: 'document',
  icon: BsMegaphone,
  title: 'News',
  fields: [
    defineField({
      title: 'Title',
      name: 'title',
      type: 'text',
      rows: 3,
    }),
    defineField({
      title: 'Date',
      name: 'date',
      type: 'date',
    }),
    defineField({
      title: 'Image',
      name: 'image',
      type: 'image',
    }),
    defineField({
      title: 'Slug',
      name: 'slug',
      type: 'slug',
      options: {
        source: (doc: any) => {
          const date = format(parseISO(doc.date), 'dd-MM-yyyy')
          const title = stringToSlug(doc.title).slice(0, 200)
          return date + '-' + title
        },
        slugify: (input) => 'nyheder/' + input,
      },
      validation: (Rule: any) => Rule.required(),
    }),
    defineField({
      title: 'Content',
      name: 'content',
      type: 'pageContent',
    }),
  ],
  preview: {
    select: {
      title: 'title',
      date: 'date',
    },
    prepare(selection: any) {
      const {title, date} = selection
      return {
        title: title,
        subtitle: format(parseISO(date), 'EEEE d MMM. yyyy'),
      }
    },
  },
})
