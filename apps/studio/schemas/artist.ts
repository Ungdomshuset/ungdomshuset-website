import {defineField, defineType} from 'sanity'
import {BandcampEmbedInput} from './Inputs/BandcampEmbedInput'

export default defineType({
  name: 'artist',
  type: 'object',
  title: 'Artist',
  fields: [
    defineField({
      title: 'Name',
      name: 'name',
      type: 'string',
    }),
    defineField({
      title: 'Origin',
      name: 'origin',
      type: 'string',
    }),
    defineField({
      title: 'Link',
      name: 'link',
      type: 'url',
    }),
    defineField({
      title: 'Bandcamp track id',
      name: 'bandcamp_track_id',
      description:
        'Hint: Paste the Wordpress embed code here. The track id will automatically be extracted.',
      type: 'string',
      components: {
        input: BandcampEmbedInput,
      },
    }),
  ],
})
