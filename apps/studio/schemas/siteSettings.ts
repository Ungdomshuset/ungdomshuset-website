import {defineField, defineType} from 'sanity'

export default defineType({
  name: 'siteSettings',
  title: 'Site Settings',
  type: 'document',
  groups: [
    {
      title: 'General',
      name: 'general',
      default: true,
    },
    {
      title: 'House rules',
      name: 'rules',
    },
  ],
  fields: [
    defineField({
      title: 'Navigation links',
      name: 'navigationLinks',
      type: 'array',
      of: [
        {
          type: 'object',
          name: 'link',
          fields: [
            defineField({
              title: 'Text',
              name: 'linkText',
              type: 'string',
              validation: (Rule) => Rule.required(),
            }),

            defineField({
              title: 'Link',
              name: 'linkRef',
              type: 'reference',
              to: [{type: 'page'}],
              validation: (Rule) => Rule.required(),
            }),

            defineField({
              title: 'Icon',
              name: 'linkIcon',
              type: 'string',
              options: {
                list: [
                  {title: 'Heart', value: 'heart'},
                  {title: 'Question mark', value: 'questionmark'},
                  {title: 'Document', value: 'document'},
                  {title: 'Music', value: 'music'},
                  {title: 'Flag', value: 'flag'},
                ],
                layout: 'dropdown',
              },
              validation: (Rule) => Rule.required(),
            }),
          ],
        },
        {
          type: 'object',
          name: 'divider',
          fields: [
            defineField({
              title: 'Divider',
              name: 'divider',
              type: 'boolean',
            }),
          ],
        },
      ],
      group: 'general',
    }),

    defineField({
      title: 'Facebook link',
      name: 'facebookUrl',
      type: 'url',
      group: 'general',
    }),

    defineField({
      title: 'Instagram link',
      name: 'instagramUrl',
      type: 'url',
      group: 'general',
    }),

    defineField({
      title: 'Mastodon link',
      name: 'mastodonUrl',
      type: 'url',
      group: 'general',
    }),

    defineField({
      title: 'Banner',
      name: 'homepageBannerRef',
      description: 'Banner shown below the calender',
      type: 'reference',
      to: [{type: 'banner'}],
      group: 'general',
    }),

    // House rules
    defineField({
      title: 'Title',
      name: 'rulesTitle',
      type: 'string',
      group: 'rules',
    }),
    defineField({
      title: 'Text',
      name: 'rulesText',
      type: 'text',
      group: 'rules',
    }),
    defineField({
      title: 'Rules',
      name: 'rulesList',
      type: 'array',
      of: [{type: 'string'}],
      group: 'rules',
    }),
    defineField({
      title: 'Link',
      name: 'rulesLinkRef',
      type: 'reference',
      to: [{type: 'page'}],
      group: 'rules',
    }),
    defineField({
      title: 'Link Text',
      name: 'rulesLinkText',
      type: 'string',
      group: 'rules',
    }),
  ],

  preview: {
    prepare() {
      return {
        title: 'Settings',
      }
    },
  },
})
