import {CiCalendar} from 'react-icons/ci'
import {defineField, defineType} from 'sanity'

export default defineType({
  name: 'event_type',
  type: 'document',
  icon: CiCalendar,
  title: 'Event type',
  fields: [
    defineField({
      title: 'Name',
      name: 'name',
      type: 'string',
    }),
    defineField({
      title: 'Sort order',
      name: 'sortOrder',
      type: 'number',
    }),
  ],
})
