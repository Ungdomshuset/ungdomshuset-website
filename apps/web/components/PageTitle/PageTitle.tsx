export const PageTitle = ({ children }: { children: React.ReactNode }) => (
  <h1 className="font-bold text-4xl lg:text-5xl text-orange-500 whitespace-pre-wrap mb-4">
    {children}
  </h1>
);
