import { PortableText } from '@portabletext/react';
import React from 'react';
import { getImageUrl } from '../../helpers/imageHelper';
import { portableTextSubpage } from '../PortableText/PortableText';

type Props = {
  title: string;
  metaText?: React.ReactNode | string;
  image: any;
  content: any;
  overlayImage?: boolean;
  slimLayout?: boolean;
};

const PageSingle = ({
  title,
  image,
  content,
  metaText,
  overlayImage = false,
  slimLayout = false
}: Props) => (
  <React.Fragment>
    {/* Image */}
    {image && (
      <img
        src={getImageUrl(image)}
        alt={title}
        className="w-full object-cover rounded-lg"
      />
    )}

    <div
      className={`bg-white md:rounded-tr-lg min-h-[160px] md:min-h-[600px] mb-20 ${
        (image && overlayImage) || slimLayout
          ? 'md:pr-10 md:mr-24 lg:pr-20 lg:mr-56'
          : 'md:pr-20'
      } ${
        image && overlayImage
          ? 'pt-8 md:pt-12 md:-translate-y-40'
          : 'pt-4 md:pt-8'
      }`}
    >
      {/* Title */}
      <h1
        className={`font-bold text-4xl lg:text-5xl text-orange-500 ${
          metaText ? 'mb-4' : 'mb-8'
        }`}
      >
        {title}
      </h1>

      {/* Meta text */}
      {metaText && (
        <p className="mb-6 border-b border-t py-2 border-orange-300">
          {metaText}
        </p>
      )}

      {/* Content */}
      {content && (
        <PortableText value={content} components={portableTextSubpage} />
      )}
    </div>
  </React.Fragment>
);

export default PageSingle;
