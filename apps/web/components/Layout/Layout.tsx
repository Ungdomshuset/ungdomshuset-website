import React from 'react';
import Footer from '../Footer/Footer';
import Navigation, { NavigationProps } from '../Navigation/Navigation';

type Props = NavigationProps & {
  showFooter?: boolean;
  children: React.ReactNode;
};

export default function Layout({
  navigation,
  showFooter = true,
  children
}: Props) {
  return (
    <React.Fragment>
      <Navigation navigation={navigation} />

      <div className="md:ml-16">
        <div className="max-w-5xl my-8 mx-4 md:px-20 lg:px-10 xl:px-0 md:mx-auto">
          {children}
        </div>

        {showFooter && <Footer />}
      </div>
    </React.Fragment>
  );
}
