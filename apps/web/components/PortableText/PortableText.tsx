import { PortableTextComponents } from '@portabletext/react';
import { getImage, getImageUrl } from '../../helpers/imageHelper';

export const portableTextSubpage: PortableTextComponents = {
  types: {
    gallery: ({ value }) => (
      <div className="grid grid-cols-2 gap-4 w-fit lg:gap-6 mb-4 lg:mb-6">
        {value.images
          .filter((image: any) => image)
          .map((image: any) => (
            <img
              className="rounded-lg border"
              src={getImage(image).format('jpg').width(300).height(300).url()}
              alt={image.alt}
              key={image._key}
            />
          ))}
      </div>
    )
  },
  block: {
    normal: ({ children }: { children?: React.ReactNode }) => (
      <p className="mb-5">{children}</p>
    ),
    h2: ({ children }: { children?: React.ReactNode }) => (
      <h2 className="font-bold text-2xl md:text-3xl text-orange-500 mb-5">
        {children}
      </h2>
    ),
    h3: ({ children }: { children?: React.ReactNode }) => (
      <h3 className="font-bold text-xl md:text-2xl mb-5">{children}</h3>
    ),
    h4: ({ children }: { children?: React.ReactNode }) => (
      <h4 className="text-lg md:text-xl mb-5">{children}</h4>
    )
  },
  marks: {
    link: ({ value, children }: { value?: any; children: React.ReactNode }) => {
      const external = !value.href.startsWith('/');
      const rel = external ? 'noreferrer noopener' : undefined;
      const target = external ? '_blank' : undefined;

      return (
        <a
          href={value.href}
          className="underline hover:no-underline"
          rel={rel}
          target={target}
        >
          {children}
        </a>
      );
    }
  }
};
