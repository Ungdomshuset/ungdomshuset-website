import { Event } from 'types';

type Props = {
  event: Event;
};

export const EventDetails = ({ event }: Props) => (
  <div className="lg:flex flex-row mb-1 mr-5">
    <span className="font-bold text-sm uppercase">{event.date}</span>

    {event.price && <span className="text-sm mx-1.5">|</span>}
    {event.price && (
      <span className="font-bold text-sm uppercase">{event.price}</span>
    )}

    <br className="md:hidden" />

    {event.place && (
      <span className="text-sm mx-1.5 hidden md:inline-block">|</span>
    )}
    {event.place && (
      <span className="font-bold text-sm uppercase">{event.place}</span>
    )}
  </div>
);
