import Link from 'next/link';
import { RecurringEvent } from 'types';
import { formatStartEndTime } from 'utilities/date';

export const RecurringEventCard = ({
  event,
  active
}: {
  event: RecurringEvent;
  active: boolean;
}) => {
  return (
    <Link
      href={`/${event.slug.current}`}
      className={`block transition-colors hover:text-orange-400 ${
        active ? 'text-orange-300' : 'text-white'
      }`}
    >
      <h3 className="underline">
        {formatStartEndTime(event.startTime, event.endTime)} | {event.place}
      </h3>
      <p className="">{event.calendarTitle}</p>
    </Link>
  );
};
