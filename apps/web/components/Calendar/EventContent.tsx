import { PortableText, PortableTextComponents } from '@portabletext/react';
import React from 'react';
import { Event } from 'types';
import { getImageUrl } from '../../helpers/imageHelper';

type Props = {
  event: Event;
};

export const EventContent = ({ event }: Props) => {
  return (
    <div className="flex flex-col-reverse md:flex-row gap-6 pt-4">
      {event.image && (
        <div className="md:w-2/6">
          <img src={getImageUrl(event.image)} alt={event.title} />
        </div>
      )}
      <div className="md:w-4/6">
        <PortableText
          value={event.content}
          components={portableEventTextComponents}
        />
      </div>
    </div>
  );
};

const portableEventTextComponents: PortableTextComponents = {
  block: {
    normal: ({ children }: { children?: React.ReactNode }) => (
      <p className="break-words mb-4">{children}</p>
    )
  },
  marks: {
    // Since the portable text in events lives inside an <a> tag,
    // we need to override the default link component to use a
    // button instead.
    link: ({ value, children }: { value?: any; children: React.ReactNode }) => {
      const url = value.href;
      const external = !url.startsWith('/');

      return (
        <button
          className="background-transparent underline hover:no-underline outline-none focus:outline-none"
          onClick={(event) => {
            event.preventDefault();

            if (external) {
              window.open(url, '_blank');
            } else {
              document.location.href = url;
            }
          }}
        >
          {children}
        </button>
      );
    }
  }
};
