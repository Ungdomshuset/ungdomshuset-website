import Link from 'next/link';
import React from 'react';
import { BsQuestionCircle, BsThreeDots } from 'react-icons/bs';
import { FaStickyNote } from 'react-icons/fa';
import { RxHamburgerMenu } from 'react-icons/rx';
import {
  FiBell,
  FiCalendar,
  FiFacebook,
  FiFlag,
  FiHeart,
  FiInstagram,
  FiMusic
} from 'react-icons/fi';
import { FaMastodon } from 'react-icons/fa';
import { AnimatePresence, motion } from 'framer-motion';

export type NavigationProps = {
  navigation: {
    navigationLinks: {
      _type: 'link' | 'divider';
      _key: string;
      linkRef: {
        slug: {
          current: string;
        };
      };
      linkText: string;
      linkIcon: string;
    }[];
    facebookUrl: string;
    instagramUrl: string;
    mastodonUrl: string;
  };
};

export default function Navigation({ navigation }: NavigationProps) {
  return (
    <React.Fragment>
      <MobileNav navigation={navigation} />

      <div
        id="drawer-navigation"
        className="hidden md:block fixed z-40 h-screen pt-3 top-0 left-0 overflow-y-auto bg-orange-500 w-20 hover:w-80 transition-all duration-300 ease-in-out"
        tabIndex={-1}
        aria-labelledby="drawer-navigation-label"
      >
        <Logo />

        <div className="py-4 mx-5 overflow-y-auto">
          <ul className="space-y-3">
            <NavItem href="/" iconElm={FiCalendar} label="Kalender" />
            <NavItem href="/nyheder" iconElm={FiBell} label="Nyheder" />
            {navigation.navigationLinks.map((link) => (
              <React.Fragment key={link._key}>
                {link._type === 'link' ? (
                  <NavItem
                    href={link.linkRef.slug.current}
                    iconName={link.linkIcon}
                    label={link.linkText}
                  />
                ) : (
                  <NavDivider />
                )}
              </React.Fragment>
            ))}
            {navigation.facebookUrl && (
              <NavItem
                href={navigation.facebookUrl}
                iconElm={FiFacebook}
                label="Find os på Facebook"
              />
            )}
            {navigation.instagramUrl && (
              <NavItem
                href={navigation.instagramUrl}
                iconElm={FiInstagram}
                label="Find os på Instragram"
              />
            )}
            {navigation.mastodonUrl && (
              <NavItem
                href={navigation.mastodonUrl}
                iconElm={FaMastodon}
                label="Find os på Mastodon"
              />
            )}
          </ul>
        </div>
      </div>
    </React.Fragment>
  );
}

const MobileNav = ({ navigation }: NavigationProps) => {
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    document.body.classList.toggle('overflow-hidden');
    setExpanded(!expanded);
  };

  return (
    <div
      className={`md:hidden fixed z-40 top-0 left-0 w-screen ${
        expanded ? 'h-screen' : ''
      }`}
    >
      <div className="flex justify-between h-[80px] bg-orange-500">
        <div className="flex items-center">
          <Logo />
        </div>

        <button
          className="flex bg-black p-5 items-center"
          onClick={handleExpandClick}
        >
          <RxHamburgerMenu color="#fff" size={40} />
        </button>
      </div>

      <AnimatePresence>
        {expanded && (
          <motion.div
            className="flex justify-between bg-white h-full w-full p-5"
            initial={{
              opacity: 0
            }}
            animate={{
              opacity: 1,
              transitionDuration: '0.25s'
            }}
            exit={{
              opacity: 0,
              transitionDuration: '0.25s'
            }}
            key="mobile-nav"
          >
            <ul className="w-full space-y-3">
              <MobileNavItem
                href="/"
                iconElm={FiCalendar}
                label="Kalender"
                onClick={handleExpandClick}
              />
              <MobileNavItem
                href="/nyheder"
                iconElm={FiBell}
                label="Nyheder"
                onClick={handleExpandClick}
              />
              {navigation.navigationLinks
                .filter((link) => link._type === 'link')
                .map((link) => (
                  <MobileNavItem
                    href={link.linkRef.slug.current}
                    iconName={link.linkIcon}
                    label={link.linkText}
                    onClick={handleExpandClick}
                    key={link._key}
                  />
                ))}
              {navigation.facebookUrl && (
                <MobileNavItem
                  href={navigation.facebookUrl}
                  iconElm={FiFacebook}
                  label="Find os på Facebook"
                  onClick={handleExpandClick}
                />
              )}
              {navigation.instagramUrl && (
                <MobileNavItem
                  href={navigation.instagramUrl}
                  iconElm={FiInstagram}
                  label="Find os på Instragram"
                  onClick={handleExpandClick}
                />
              )}
              {navigation.mastodonUrl && (
                <MobileNavItem
                  href={navigation.mastodonUrl}
                  iconElm={FaMastodon}
                  label="Find os på Mastodon"
                  onClick={handleExpandClick}
                />
              )}
            </ul>
          </motion.div>
        )}
      </AnimatePresence>
    </div>
  );
};

const Logo = () => (
  <Link
    href="/"
    className="inline-block w-14 h-14 ml-3"
    aria-label="Gå til forsiden"
  >
    <svg
      className="w-full h-full"
      width="100"
      height="100"
      viewBox="0 0 100 100"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M100 50C100 77.6142 77.6142 100 50 100C22.3858 100 0 77.6142 0 50C0 22.3858 22.3858 0 50 0C77.6142 0 100 22.3858 100 50Z"
        fill="black"
      />
      <path
        d="M46.4802 36.4389L46.3821 37.8208L45.9776 42.8109C45.9622 43.003 45.9567 43.1958 45.9461 43.3986C46.0828 43.4559 46.1825 43.3859 46.2866 43.345L52.1437 41.0464L56.0446 39.5049C56.2741 39.4131 56.4953 39.388 56.7398 39.4199C57.2271 39.4834 57.7176 39.5221 58.2067 39.5713L58.5588 39.6085C58.4614 39.746 58.402 39.842 58.3309 39.9284L54.1211 45.0336C53.9304 45.2643 53.7637 45.5077 53.6152 45.7677L50.837 50.6115L50.6006 51.0361C50.6244 51.0547 50.6351 51.0663 50.6483 51.0723C50.66 51.0777 50.6764 51.0821 50.6875 51.0782L58.5055 48.292C58.5215 48.1861 58.5418 48.0997 58.5465 48.0124L59.1783 36.092L59.3907 31.8051C59.4022 31.5594 59.4614 31.3515 59.6188 31.1493C59.9257 30.755 60.096 30.2909 60.1698 29.7981L60.527 27.2766L60.7268 25.8157C60.7484 25.6692 60.7871 25.5232 60.8361 25.3833C60.9246 25.131 61.1118 25.0013 61.3799 25C61.5129 24.9994 61.6459 25.012 61.7996 25.0196C61.8292 25.1049 61.8657 25.1836 61.8837 25.2663L62.6036 28.583C62.6394 28.746 62.6391 28.902 62.6219 29.0679C62.5137 30.1102 62.5287 31.151 62.6973 32.187C62.7183 32.3159 62.7572 32.442 62.7967 32.606C62.9145 32.521 63.011 32.458 63.1003 32.3859C63.475 32.0832 63.8901 31.972 64.3531 32.1368C64.6307 32.2356 64.8999 32.3577 65.196 32.4788C65.2257 32.1004 65.0828 31.7548 65.0113 31.3994L64.7798 30.3356C65.0979 30.5118 66.5264 31.6765 66.8498 32.0157L66.747 34.1944L66.7008 36.8764L66.6836 45.1938C66.6834 45.34 66.6921 45.4862 66.6966 45.6305C67.0927 45.6432 70.2288 45.1032 73.035 44.544C73.0572 44.4171 72.9484 44.4053 72.8751 44.3667C71.7158 43.7558 70.5196 43.2237 69.3182 42.7029L68.2523 42.2444C68.159 42.204 68.0685 42.1572 67.9383 42.0949C68.2995 41.9335 68.6502 41.9844 68.9811 41.9216C69.3021 41.8607 69.6315 41.8435 69.9574 41.8086L70.9796 41.7018C71.3055 41.6666 71.6344 41.6472 71.9558 41.5874C72.2899 41.5253 72.6417 41.5743 73.0024 41.42C72.4005 41.1261 71.7852 40.9562 71.1472 40.6631C71.317 40.6272 71.4229 40.5927 71.5311 40.5838L73.8486 40.4049C74.5361 40.3663 75.2182 40.4419 75.8891 40.5972L86.9351 43.1582C87.0799 43.1918 87.2236 43.2303 87.3683 43.2666C87.4213 43.3937 87.3066 43.4141 87.2403 43.4553L82.9496 46.1142L72.0022 52.8725C71.8651 52.9571 71.7239 53.035 71.5425 53.1405C71.5377 53.0297 71.5264 52.9629 71.5334 52.898C71.5671 52.5871 71.5801 52.2707 71.65 51.9677C71.7162 51.6806 71.8107 51.3894 71.9515 51.1322C72.2586 50.5713 72.5993 50.0287 72.9339 49.4833C73.0726 49.2573 73.2306 49.0431 73.3888 48.8104C73.0424 48.8526 67.3848 50.5943 66.4373 50.948C66.3438 51.4724 66.326 52.022 66.2404 52.5841C66.7985 52.7392 67.376 52.6885 67.9308 52.8695C67.9204 53.2341 67.8705 53.5821 67.8063 53.9458C67.4249 54.0176 67.0657 53.8237 66.7019 53.889C66.6813 53.918 66.6537 53.9401 66.6488 53.9664C66.238 56.1454 65.6496 58.2823 65.0914 60.4617C65.3427 60.4247 65.5257 60.3042 65.7234 60.2288C65.9457 60.144 66.164 60.0488 66.3847 59.9598L67.0071 59.7136C67.2135 59.6359 67.4223 59.5648 67.6461 59.4852C67.8297 59.8193 67.9051 60.1702 68.0525 60.4941C67.9869 60.6636 67.8391 60.7491 67.723 60.8576C66.2271 62.255 64.8594 63.7677 63.6265 65.4017C63.4923 65.5796 63.3684 65.7665 63.2546 65.9582C60.5685 70.484 56.6622 73.3655 51.5511 74.5938C49.8042 75.0136 48.0221 75.1569 46.2248 74.9992L42.3969 74.6499L41.3741 74.5488C41.3168 74.5428 41.2607 74.5254 41.1025 74.4916L44.4341 74.1036C44.528 74.1006 44.6217 74.092 44.7145 74.0778L44.7148 74.0213L44.4388 73.9952C44.4388 73.9952 44.4368 74.0008 44.4111 73.9763C44.2513 73.9376 44.1173 73.9232 43.9833 73.9088C42.62 73.6376 41.3122 73.2045 40.0668 72.5831C39.1178 72.1096 38.2248 71.5494 37.3973 70.8867C37.1979 70.727 36.99 70.5916 36.7496 70.5016C36.541 70.4236 36.3421 70.32 36.134 70.2405C35.7482 70.0932 35.4374 69.8466 35.1676 69.5409C34.8405 69.1705 34.5766 68.7566 34.3382 68.3263C33.8685 67.4785 33.5001 66.5853 33.1623 65.6785C32.4488 63.7634 31.9248 61.7974 31.5777 59.7841C31.4996 59.3311 31.4403 58.8749 31.3709 58.4103C31.0512 58.3983 30.77 58.4967 30.4846 58.5497C29.8122 58.6748 29.1456 58.8317 28.4731 58.956C28.2466 58.9978 28.0499 59.0858 27.8549 59.1958L21.6744 62.6791L19.2039 64.0541C19.0941 64.0711 19.047 64.0746 19 64.078L20.3844 61.4021C20.3291 61.3194 20.2891 61.2164 20.2146 61.1555C19.8801 60.8823 19.7386 60.5077 19.675 60.1062C19.5692 59.4387 19.6165 58.7699 19.7227 58.1049C19.7247 58.0925 19.74 58.0822 19.7703 58.0445C20.0648 58.1266 20.3162 58.3555 20.6432 58.436C20.7395 58.0345 20.6638 57.6639 20.6596 57.296C20.6556 56.94 20.6246 56.5844 20.6049 56.217C20.7195 56.0966 20.8881 56.0763 21.0387 56.0238C21.9104 55.7201 22.7645 55.375 23.5947 54.9703C23.7974 54.8715 23.982 54.7548 24.153 54.6071C24.9009 53.9612 25.7271 53.433 26.6116 52.994C26.7979 52.9016 26.9821 52.8038 27.1606 52.6972C28.057 52.1616 28.9895 51.7009 29.9695 51.3396C30.1353 51.2785 30.3137 51.2152 30.486 51.2158C30.8167 51.2171 31.0953 51.1054 31.3572 50.9229C31.4175 50.8809 31.4791 50.8406 31.5418 50.7984L31.6221 49.7242L31.9299 44.8618C31.9602 44.386 31.9769 43.9084 32.0334 43.4356C32.0656 43.1664 32.0056 42.9461 31.8761 42.7166L30.2923 39.8668L27.9826 35.7291C27.9516 35.659 27.9288 35.6409 27.9061 35.6229C27.9061 35.6229 27.8977 35.6159 27.9033 35.5864C27.9336 35.5202 27.9584 35.4835 27.9832 35.4467L31.9749 38.6123C32.0771 38.6933 32.1832 38.7695 32.2791 38.8415C32.3964 38.793 32.3649 38.7059 32.3701 38.6412L32.6426 35.2104L32.895 32.1816C32.9245 31.8319 33.1964 31.5818 33.5454 31.5864C33.7923 31.5896 34.0083 31.6903 34.2086 31.8238C34.3193 31.8976 34.4193 31.9873 34.5156 32.0631L36.8058 30.9564C37.3845 29.3503 37.7743 27.6393 38.3133 25.908C38.3988 26.0768 38.3798 26.2051 38.3822 26.3243L38.46 30.526C38.5298 34.1173 38.5337 37.7089 38.5008 41.3005L38.4037 46.5748C38.4024 46.6451 38.4129 46.7155 38.4205 46.8226C38.5275 46.7751 38.6086 46.7469 38.6821 46.7052C39.8729 46.0288 41.1258 45.4681 42.4237 45.031C42.6235 44.9636 42.7465 44.8538 42.8473 44.6744C43.6883 43.1789 44.3949 41.6235 44.9807 40.0107C45.4074 38.8359 45.8594 37.6703 46.3385 36.4712C46.4113 36.4409 46.4457 36.4398 46.4802 36.4389ZM50.5856 56.6736L50.2958 56.79L42.7626 59.6984C42.4534 59.8185 42.1649 59.8665 41.8638 59.7011C41.8416 59.6889 41.8069 59.6995 41.7747 59.6995L40.1094 61.9002C40.1026 61.9092 40.0855 61.9151 40.0735 61.9146C40.0595 61.9139 40.0458 61.9047 40.0106 61.8907L40.0107 61.6852L40.0387 59.4056C40.0377 59.1246 40.1178 58.8941 40.2707 58.6608L46.3816 49.3111C46.4672 49.18 46.5478 49.0455 46.6324 48.9097C46.4938 48.8438 46.4116 48.9391 46.3251 48.9792L41.5942 51.1881C41.4207 51.2697 41.2556 51.3751 41.1003 51.4882L38.3257 53.5233C38.219 53.6018 38.117 53.6867 38.013 53.7685C37.9065 55.2419 37.9223 56.6981 38.1842 58.1436C38.5564 60.1982 39.423 61.9871 41.021 63.3802C41.1797 63.5186 41.343 63.6383 41.5341 63.7291C42.5447 64.2092 43.5913 64.5883 44.6705 64.8825C45.5339 65.1178 46.4192 65.2408 47.2939 65.4181C47.6331 65.4869 47.9759 65.5413 48.3504 65.6373C48.1959 65.7885 48.0268 65.8079 47.8872 65.8821C47.8301 65.9124 47.7439 65.9239 47.7588 66.0385C48.7441 65.997 49.7209 65.8926 50.6722 65.6314C51.3454 65.4465 52.0018 65.2003 52.6744 65.0125C53.1379 64.8831 53.5126 64.6359 53.834 64.2932C53.9968 64.1195 54.1657 63.9474 54.3026 63.7539L55.5177 61.9883C55.7206 61.6949 55.9205 61.3981 56.1443 61.1211C56.2596 60.9783 56.3493 60.8317 56.4019 60.6599C56.6897 59.7193 56.9244 58.7664 57.0571 57.7902C57.0804 57.6183 57.105 57.4458 57.1601 57.2702L57.7168 57.3041C57.738 57.2502 57.7639 57.2113 57.7679 57.1703L58.1067 53.6565C58.1106 53.6158 58.091 53.5729 58.076 53.5005L53.4177 55.0565C53.3502 55.0792 53.2923 55.1306 53.1842 55.1967C53.6232 55.1613 54.0103 55.1119 54.4003 55.1322C54.3981 55.1743 54.4016 55.2002 54.3959 55.2024L50.5856 56.6736Z"
        fill="white"
      />
    </svg>
  </Link>
);

const NavDivider = () => (
  <li>
    <BsThreeDots className="w-6 h-6 text-black ml-2" />
  </li>
);

type NavItemProps = {
  href: string;
  iconElm?: React.FC<{ className: string }>;
  iconName?: string;
  label: string;
};

const NavItem = ({ href, iconElm, iconName, label }: NavItemProps) => {
  // add forward slash to href if it's not an external link
  const url =
    !href.startsWith('http') && !href.startsWith('/')
      ? (href = `/${href}`)
      : href;
  const target = href.startsWith('http') ? '_blank' : undefined;
  const icon = iconElm || getIcon(iconName);

  return (
    <li>
      <Link
        href={url}
        className="flex gap-x-5 items-center w-full py-2 px-2 text-base font-normal text-gray-900 transition duration-75 rounded-lg group hover:bg-orange-600 hover:text-white "
        target={target}
      >
        {icon && (
          <span className="flex-shrink-0">
            {icon({
              className: 'flex-shrink-0 w-6 h-6 text-white'
            })}
          </span>
        )}

        <span className="text-white flex-1 text-left whitespace-nowrap">
          {label}
        </span>
      </Link>
    </li>
  );
};

type MobileNavItemProps = NavItemProps & {
  onClick: () => void;
};

const MobileNavItem = ({
  href,
  iconElm,
  iconName,
  label,
  onClick
}: MobileNavItemProps) => {
  // add forward slash to href if it's not an external link
  const url =
    !href.startsWith('http') && !href.startsWith('/')
      ? (href = `/${href}`)
      : href;
  const target = href.startsWith('http') ? '_blank' : undefined;
  const icon = iconElm || getIcon(iconName);

  return (
    <li>
      <Link
        href={url}
        className="flex gap-x-5 items-center pt-2 pb-5 px-2 border-b hover:text-orange-500"
        onClick={onClick}
        target={target}
      >
        {icon && (
          <span className="flex-shrink-0">
            {icon({
              className: 'flex-shrink-0 w-6 h-6 text-orange-500'
            })}
          </span>
        )}

        <span className="font-semibold">{label}</span>
      </Link>
    </li>
  );
};

const getIcon = (iconName?: string) => {
  switch (iconName) {
    case 'heart':
      return FiHeart;
    case 'questionmark':
      return BsQuestionCircle;
    case 'flag':
      return FiFlag;
    case 'document':
      return FaStickyNote;
    case 'music':
      return FiMusic;
  }

  return null;
};
