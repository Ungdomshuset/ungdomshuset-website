import { GetStaticPaths } from 'next';
import React, { ReactElement } from 'react';
import { Page as PageType } from 'types';
import Layout from '../components/Layout/Layout';
import PageSingle from '../components/Layout/PageSingle';
import { NavigationProps } from '../components/Navigation/Navigation';
import { Seo } from '../components/Seo/Seo';
import sanityClient from '../sanityClient';
import { NextPageWithLayout } from './_app';

type Props = NextPageWithLayout & {
  page: PageType;
  navigation: NavigationProps;
};

const Page = ({ page }: Props) => {
  return (
    <React.Fragment>
      <Seo
        pageTitle={page.title}
        pageUrl={page.slug.current}
        pageImage={page.image}
        seo={page.seo}
      />

      <PageSingle
        title={page.title}
        image={page.image}
        content={page.content}
        overlayImage
        slimLayout
      />

      {/* 
      {page.image && (
        <img
          src={getImageUrl(page.image)}
          alt={page.title}
          className="w-full object-cover rounded-lg"
        />
      )}

      <div
        className={`bg-white md:rounded-tr-lg pt-8 md:pt-12 md:pr-10 md:mr-24 lg:pr-20 lg:mr-56 min-h-[160px] md:min-h-[600px] ${
          page.image ? 'md:-translate-y-40' : ''
        }`}
      >
        <h1 className="font-bold text-4xl lg:text-5xl text-orange-500 mb-4">
          {page.title}
        </h1>

        {page.content && (
          <PortableText value={page.content} components={portableTextSubpage} />
        )}
      </div> */}
    </React.Fragment>
  );
};

Page.getLayout = function getLayout(page: ReactElement) {
  return (
    <div className="md:mt-20">
      <Layout navigation={page.props.navigation}>{page}</Layout>
    </div>
  );
};

export default Page;

export async function getStaticProps(props: any) {
  const query = `
    {
      'page':*[_type == "page" && slug.current == $slug][0]
      {
        _id,
        title,
        slug,
        image,
        content,
        seo
      },

      'navigation': *[_type == "siteSettings"][0]
      {
        navigationLinks[]{
          _type,
          _key,
          linkRef->{
            slug
          },
          linkText,
          linkIcon
        },
        facebookUrl,
        instagramUrl,
        mastodonUrl
      }
    }
  `;
  const result = await sanityClient.fetch<{
    page: PageType;
    navigation: NavigationProps;
  }>(query, {
    slug: props.params.slug
  });

  if (!result.page) {
    return {
      notFound: true
    };
  }

  return {
    props: {
      page: result.page,
      navigation: result.navigation
    }
  };
}

type PageSlug = {
  slug: {
    current: string;
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  // Skip the homepage
  const query = `
  *[_type == "page" && slug != null && slug.current != "/"]
  {
    slug
  }
  `;

  const pages = await sanityClient.fetch<PageSlug[]>(query);
  const paths = pages.map((page: PageSlug) => ({
    params: {
      slug: page.slug.current
    }
  }));

  return { paths, fallback: false };
};
