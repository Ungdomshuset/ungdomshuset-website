import React, { ReactElement, useEffect } from 'react';
import { Event, RecurringEvent } from 'types';
import { EventArtistsPreview } from '../../components/Calendar/EventArtistsPreview';
import { EventArtistsFull } from '../../components/Calendar/EventArtistsFull';
import { EventContent } from '../../components/Calendar/EventContent';
import { EventDetails } from '../../components/Calendar/EventDetails';
import { EventTitle } from '../../components/Calendar/EventTitle';
import { RecurringEvents } from '../../components/Calendar/RecurringEvents';
import Layout from '../../components/Layout/Layout';
import {
  TwoColumns,
  TwoColumnsEnd,
  TwoColumnsStart
} from '../../components/Layout/TwoColumns';
import { NavigationProps } from '../../components/Navigation/Navigation';
import { Seo } from '../../components/Seo/Seo';
import { decodeHtmlEntities, formatEventDate } from '../../helpers/eventHelper';
import sanityClient from '../../sanityClient';
import { NextPageWithLayout } from '../_app';

type Props = NextPageWithLayout & {
  event: Event;
  recurringEvents: RecurringEvent[];
};

const Page = ({ event, recurringEvents }: Props) => {
  useEffect(() => {
    window.scrollTo(0, 1);
  }, []);

  return (
    <React.Fragment>
      <Seo
        pageTitle={event.title}
        pageUrl={event.slug.current}
        pageImage={event.image}
      />

      <TwoColumns>
        <TwoColumnsStart>
          <div className="bg-white border border-black rounded-lg p-5 relative">
            <EventDetails event={event} />
            <EventTitle title={event.title} as="h1" />
            <EventArtistsPreview artists={event.artists} />
            <EventContent event={event} />
            <EventArtistsFull artists={event.artists} />
          </div>
        </TwoColumnsStart>
        <TwoColumnsEnd>
          <RecurringEvents events={recurringEvents} />
        </TwoColumnsEnd>
      </TwoColumns>
    </React.Fragment>
  );
};

Page.getLayout = function getLayout(page: ReactElement) {
  return (
    <div className="md:mt-20">
      <Layout navigation={page.props.navigation}>{page}</Layout>
    </div>
  );
};

export default Page;

export async function getServerSideProps(props: any) {
  const query = `
    {
      'event': *[_type == "event" && slug.current == $slug][0]
      {
        _id,
        title,
        date,
        price,
        place,
        event_type,
        slug,
        image,
        artists,
        content
      },

      'recurringEvents': *[_type == "recurring_event"]
      {
        _id,
        calendarTitle,
        weekday,
        startTime,
        endTime,
        place,
        slug
      } | order(weekday asc, startTime asc),

      'navigation': *[_type == "siteSettings"][0]
      {
        navigationLinks[]{
          _type,
          _key,
          linkRef->{
            slug
          },
          linkText,
          linkIcon
        },
        facebookUrl,
        instagramUrl,
        mastodonUrl
      }
    }
  `;

  const { event, recurringEvents, navigation } = await sanityClient.fetch<{
    event: Event;
    recurringEvents: RecurringEvent[];
    navigation: NavigationProps;
  }>(query, {
    slug: `kalender/${props.params.slug}`
  });

  if (!event) {
    return {
      notFound: true
    };
  }

  return {
    props: {
      event: {
        ...event,
        title: decodeHtmlEntities(event.title),
        // format at build time to avoid hydration mismatch
        date: formatEventDate(event.date)
      },
      recurringEvents,
      navigation
    }
  };
}
