import endOfYear from 'date-fns/endOfYear';
import format from 'date-fns/format';
import startOfYear from 'date-fns/startOfYear';
import { GetStaticPaths } from 'next';
import React, { ReactElement } from 'react';
import { Event, RecurringEvent } from 'types';
import { CalendarArchive } from '../../../../components/Calendar/CalendarArchive';
import Layout from '../../../../components/Layout/Layout';
import { NavigationProps } from '../../../../components/Navigation/Navigation';
import { Seo } from '../../../../components/Seo/Seo';
import { FIRST_CALENDAR_YEAR } from '../../../../constants';
import {
  decodeHtmlEntities,
  formatEventDate
} from '../../../../helpers/eventHelper';
import { getImageUrl } from '../../../../helpers/imageHelper';
import sanityClient from '../../../../sanityClient';
import { NextPageWithLayout } from '../../../_app';

type Props = NextPageWithLayout & {
  currentYear: number;
  pageImage: {
    asset: {
      _ref: string;
      _type: string;
    };
  };
  events: Event[];
  recurringEvents: RecurringEvent[];
};

const CalendarYear = (props: Props) => {
  return (
    <React.Fragment>
      <Seo
        pageTitle={`Kalender arkiv ${props.currentYear}`}
        pageUrl={`/kalender/arkiv/${props.currentYear}`}
      />

      <CalendarArchive
        currentYear={props.currentYear}
        events={props.events}
        recurringEvents={props.recurringEvents}
      />
    </React.Fragment>
  );
};

CalendarYear.getLayout = function getLayout(page: ReactElement) {
  return (
    <React.Fragment>
      {page.props.pageImage && (
        <div className="relative md:mb-10 xl:mb-20">
          <img
            src={getImageUrl(page.props.pageImage)}
            alt="Kalender hero image"
            className="w-full object-cover"
          />
        </div>
      )}

      <Layout navigation={page.props.navigation}>{page}</Layout>
    </React.Fragment>
  );
};

export default CalendarYear;

export async function getStaticProps(props: any) {
  const requestedYearStart = new Date(props.params.year, 0, 1);
  const currentYear = new Date().getFullYear();
  const startDate = format(startOfYear(requestedYearStart), 'yyyy-MM-dd');
  // If the current year is requested, we want to show events up until today
  const endDate =
    props.params.year == currentYear
      ? format(new Date(), 'yyyy-MM-dd')
      : format(endOfYear(requestedYearStart), 'yyyy-MM-dd');

  const query = `
  {
    'page': *[_type == "page" && slug.current == "/"]
    {
      _id,
      title,
      slug,
      image,
      content,
      seo
    }[0],

    'events': *[_type == "event" && date >= $startDate && date <= $endDate]
    {
      _id,
      title,
      date,
      price,
      place,
      event_type,
      slug,
      image,
      artists,
      content
    } | order(date asc),

    'recurringEvents': *[_type == "recurring_event"]
    {
      _id,
      calendarTitle,
      weekday,
      startTime,
      endTime,
      place,
      slug
    } | order(weekday asc, startTime asc),

    'navigation': *[_type == "siteSettings"][0]
    {
      navigationLinks[]{
        _type,
        _key,
        linkRef->{
          slug {
            current
          }
        },
        linkText,
        linkIcon
      },
      facebookUrl,
      instagramUrl,
      mastodonUrl
    }
  }
  `;

  const result = await sanityClient.fetch<{
    page: {
      image: {
        asset: {
          url: string;
        };
      };
    };
    events: Event[];
    recurringEvents: RecurringEvent[];
    navigation: NavigationProps;
  }>(query, {
    startDate,
    endDate
  });

  const events = result.events.map((event) => ({
    ...event,
    title: decodeHtmlEntities(event.title),
    // format at build time to avoid hydration mismatch
    date: formatEventDate(event.date)
  }));

  return {
    props: {
      currentYear: props.params.year,
      pageImage: result.page.image,
      events,
      recurringEvents: result.recurringEvents,
      navigation: result.navigation
    },
    revalidate: 60 * 10 // 10 minutes
  };
}

export const getStaticPaths: GetStaticPaths = async () => {
  // Create array of years from 2008 to current year
  const firstYear = FIRST_CALENDAR_YEAR;
  const year = Array.from(
    { length: new Date().getFullYear() - firstYear + 1 },
    (_, i) => i + firstYear
  );

  // Turn years into array of paths
  const paths = year.map((year) => ({
    params: { year: year.toString() }
  }));

  return {
    paths,
    fallback: false
  };
};
