import Head from 'next/head';
import React, { ReactElement } from 'react';
import { PageError } from '../components/Error/PageError';
import Layout from '../components/Layout/Layout';
import { NavigationProps } from '../components/Navigation/Navigation';
import sanityClient from '../sanityClient';

const Custom404 = () => {
  return (
    <React.Fragment>
      <Head>
        <title>500 - Der opstod en grim fejl!</title>
      </Head>
      <PageError supTitle="Computer says" title="500 major malfunction" />
    </React.Fragment>
  );
};

Custom404.getLayout = function getLayout(page: ReactElement) {
  return <Layout navigation={page.props.navigation}>{page}</Layout>;
};

export default Custom404;

export async function getStaticProps(props: any) {
  const query = `
    {
      'navigation': *[_type == "siteSettings"][0]
      {
        navigationLinks[]{
          _type,
          _key,
          linkRef->{
            slug
          },
          linkText,
          linkIcon
        },
        facebookUrl,
        instagramUrl,
        mastodonUrl
      }
    }
  `;
  const { navigation } = await sanityClient.fetch<{
    navigation: NavigationProps;
  }>(query);

  return {
    props: {
      navigation
    }
  };
}
