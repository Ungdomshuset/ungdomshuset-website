import Link from 'next/link';
import React, { ReactElement } from 'react';
import { NewsItem, RecurringEvent } from 'types';
import { RecurringEvents } from '../../components/Calendar/RecurringEvents';
import Layout from '../../components/Layout/Layout';
import {
  TwoColumns,
  TwoColumnsEnd,
  TwoColumnsStart
} from '../../components/Layout/TwoColumns';
import { NavigationProps } from '../../components/Navigation/Navigation';
import { Seo } from '../../components/Seo/Seo';
import { formatNewsDate } from '../../helpers/eventHelper';
import { getImage } from '../../helpers/imageHelper';
import { blocksToText } from '../../helpers/portableTextHelper';
import sanityClient from '../../sanityClient';
import { NextPageWithLayout } from '../_app';

type Props = NextPageWithLayout & {
  news: NewsItem[];
  recurringEvents: RecurringEvent[];
};

const Page = ({ news, recurringEvents }: Props) => {
  // Bump

  return (
    <React.Fragment>
      <Seo pageTitle="Nyheder" pageUrl="/nyheder" />

      <h1 className="font-bold text-4xl md:text-5xl text-orange-500 mb-6 lg:mb-12">
        Nyheder
      </h1>

      <TwoColumns>
        <TwoColumnsStart>
          <NewsList news={news} />
        </TwoColumnsStart>
        <TwoColumnsEnd>
          <RecurringEvents events={recurringEvents} />
        </TwoColumnsEnd>
      </TwoColumns>
    </React.Fragment>
  );
};

const NewsList = ({ news }: { news: NewsItem[] }) => (
  <div className="flex flex-col gap-y-6 mb-10">
    {news.map((item, index) => {
      const contentText = blocksToText(item.content);
      const abstractLength = 200;
      const abstractText =
        contentText.substring(0, abstractLength) +
        (contentText.length > abstractLength ? '...' : '');

      return (
        <div
          className={`flex flex-col lg:flex-row gap-x-10 ${
            index ? 'border-t border-black pt-6' : ''
          } }`}
          key={item._id}
        >
          {item.image && (
            <div className="shrink-0 mb-4 lg:mb-0">
              <Link href={item.slug.current}>
                <img
                  className="rounded-lg border"
                  src={getImage(item.image).width(200).height(200).url()}
                  alt={item.title}
                />
              </Link>
            </div>
          )}

          <div>
            <h2 className="font-bold text-2xl text-orange-500 mb-1">
              <Link className="hover:underline" href={item.slug.current}>
                {item.title}
              </Link>
            </h2>
            <p className="text-sm mb-2">{item.date}</p>
            <div className="mb-1">{abstractText}</div>
            <Link href={item.slug.current}>
              <strong className="hover:underline">Læs mere</strong>
            </Link>
          </div>
        </div>
      );
    })}
  </div>
);

Page.getLayout = function getLayout(page: ReactElement) {
  return (
    <div className="md:mt-20">
      <Layout navigation={page.props.navigation}>{page}</Layout>
    </div>
  );
};

export default Page;

export async function getStaticProps() {
  const query = `
    {
      'news': *[_type == "news" && slug != null]
      {
        _id,
        title,
        date,
        image,
        slug,
        content
      } | order(date desc),

      'recurringEvents': *[_type == "recurring_event"]
      {
        _id,
        calendarTitle,
        weekday,
        startTime,
        endTime,
        place,
        slug
      } | order(weekday asc, startTime asc),

      'navigation': *[_type == "siteSettings"][0]
      {
        navigationLinks[]{
          _type,
          _key,
          linkRef->{
            slug
          },
          linkText,
          linkIcon
        },
        facebookUrl,
        instagramUrl,
        mastodonUrl
      }
    }
  `;

  const result = await sanityClient.fetch<{
    news: NewsItem[];
    recurringEvents: RecurringEvent[];
    navigation: NavigationProps;
  }>(query);

  const news = result.news.map((item) => ({
    ...item,
    date: formatNewsDate(item.date)
  }));

  return {
    props: {
      news,
      recurringEvents: result.recurringEvents,
      navigation: result.navigation
    }
  };
}
