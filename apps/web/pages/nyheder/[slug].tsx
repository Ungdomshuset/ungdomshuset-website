import { GetStaticPaths } from 'next';
import React, { ReactElement } from 'react';
import { NewsItem, RecurringEvent } from 'types';
import { RecurringEvents } from '../../components/Calendar/RecurringEvents';
import Layout from '../../components/Layout/Layout';
import PageSingle from '../../components/Layout/PageSingle';
import {
  TwoColumns,
  TwoColumnsEnd,
  TwoColumnsStart
} from '../../components/Layout/TwoColumns';
import { NavigationProps } from '../../components/Navigation/Navigation';
import { Seo } from '../../components/Seo/Seo';
import { formatNewsDate } from '../../helpers/eventHelper';
import sanityClient from '../../sanityClient';
import { NextPageWithLayout } from '../_app';

type Props = NextPageWithLayout & {
  current: NewsItem;
  recurringEvents: RecurringEvent[];
};

const Page = ({ current, recurringEvents }: Props) => {
  return (
    <React.Fragment>
      <Seo
        pageTitle={current.title}
        pageUrl={current.slug.current}
        pageImage={current.image}
      />

      <TwoColumns>
        <TwoColumnsStart>
          <PageSingle
            title={current.title}
            image={current.image}
            metaText={current.date}
            content={current.content}
          />

          {/* {current.image && (
            <img
              src={getImageUrl(current.image)}
              alt={current.title}
              className="w-full object-cover border rounded-lg mb-8 md:mb-12"
            />
          )}

          <div className="bg-white md:pr-20 min-h-[160px] md:min-h-[600px]">
            <PageTitle>{current.title}</PageTitle>

            <p className="mb-6 border-b border-t py-2 border-orange-300">
              {current.date}
            </p>

            {current.content && (
              <PortableText
                value={current.content}
                components={portableTextSubpage}
              />
            )}
          </div> */}
        </TwoColumnsStart>
        <TwoColumnsEnd>
          <RecurringEvents events={recurringEvents} />
        </TwoColumnsEnd>
      </TwoColumns>
    </React.Fragment>
  );
};

Page.getLayout = function getLayout(page: ReactElement) {
  return (
    <div className="md:mt-20">
      <Layout navigation={page.props.navigation}>{page}</Layout>
    </div>
  );
};

export default Page;

export async function getStaticProps(props: any) {
  const query = `
    {
      'current': *[_type == "news" && slug.current == $slug][0]
      {
        _id,
        title,
        date,
        image,
        slug,
        content
      },

      'recurringEvents': *[_type == "recurring_event"]
      {
        _id,
        calendarTitle,
        weekday,
        startTime,
        endTime,
        place,
        slug
      } | order(weekday asc, startTime asc),

      'navigation': *[_type == "siteSettings"][0]
      {
        navigationLinks[]{
          _type,
          _key,
          linkRef->{
            slug
          },
          linkText,
          linkIcon
        },
        facebookUrl,
        instagramUrl,
        mastodonUrl
      }
    }
  `;
  const { current, recurringEvents, navigation } = await sanityClient.fetch<{
    current: NewsItem;
    recurringEvents: RecurringEvent[];
    navigation: NavigationProps;
  }>(query, {
    slug: `nyheder/${props.params.slug}`
  });

  if (!current) {
    return {
      notFound: true
    };
  }

  return {
    props: {
      current: {
        ...current,
        date: formatNewsDate(current.date)
      },
      recurringEvents,
      navigation
    }
  };
}

type ResultSlug = {
  slug: {
    current: string;
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const query = `
  *[_type == "news"]
  {
    slug
  }
  `;

  const slugs = await sanityClient.fetch<ResultSlug[]>(query);
  const paths = slugs.map((slug: ResultSlug) => ({
    params: {
      slug: slug.slug.current.replace('nyheder/', '')
    }
  }));

  return { paths, fallback: false };
};
